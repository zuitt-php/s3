<?php

//[Objects as Variables]

$buildingObj = (object)[
	'name' => 'Caswynn building',
	'floors' => 8,
	'address' => (object)[
		'barangay' => 'Sacred Heart',
		'city' => 'Quezon City',
		'country' => 'Philippines'
	]
];

//Objects from Classes

class Building {
	public $name;
	public $floors;
	public $address;

	//constructor

	public function __construct($name1, $floors1, $address1){
		$this->name = $name1;
		$this->floors = $floors1;
		$this->address = $address1;
	}

	//methods

	public function printName(){
		return "The name of the building is $this->name";
	}
	//you cant call printname since building has no value but variables
}

/*
Constructor is used during the creation of an object to provide the initial values of each property
*/

//[Inheritance and Polymorphism]

class Condominium extends Building {
	// $name $floors $address and printName() are inherited from the Building class to this class.

	//clss properties is inherited

	public function printName(){
		return "The name of this condominium is $this->name.";
	}
	//polymorphism overwrites the class
}

$building = new Building('Caswynn Building', 8, 'Timog Avenue, Quezon City, Philippines');
$building2 = new Building('Manulife Building', 51, 'Commonwealth Avenue, Quezon City, Philippines');

$condominium = new Condominium('Enzo Condo', 5, 'Buendia Ave, Makati City, Philippines');





