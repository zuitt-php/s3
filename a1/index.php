<?php require_once "./code.php"; ?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Activity 3</title>
</head>
<body>

<h1>Person</h1>
<p><?= $leader1->printName(); ?></p>

<br/><br/><br/>

<h1>Developer</h1>
<p><?= $leader2->printName(); ?></p>

<br/><br/><br/>

<h1>Engineer</h1>
<p><?= $leader3->printName(); ?></p>

</body>
</html>