<?php

class Person {
	public $firstName;
	public $middleName;
	public $lastName;

	public function __construct($name1, $name2, $name3){
		$this->firstName = $name1;
		$this->middleName = $name2;
		$this->lastName = $name3;
	}

	public function printName(){
		return "Your full name is $this->firstName $this->middleName $this->lastName";
	}

};

$leader1 = new Person('Tsukasa', 'K.', 'Shishio');

class Developer extends Person {

	public function printName(){
		return "Your name is $this->firstName $this->middleName $this->lastName and you are a developer.";
	}
};

$leader2 = new Developer('Grishia', 'R.', 'Yeager');

class Engineer extends Person {

	public function printName(){
		return "You are an engineer named $this->firstName $this->middleName $this->lastName.";
	}
};

$leader3 = new Engineer('Edward', 'H.', 'Elric');
